package cn.com.smart.form.list.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cn.com.smart.form.enums.FormDataSourceType;
import cn.com.smart.form.list.bean.OptionListFieldProp;
import cn.com.smart.web.bean.LabelValue;
import com.mixsmart.enums.YesNoType;
import com.mixsmart.utils.CollectionUtils;
import com.mixsmart.utils.StringUtils;

import cn.com.smart.form.list.bean.AbstractListFieldProp;

/**
 * 表单列表解析助手类
 * @author lmq
 *
 */
public class FormListParseHelper {

    /**
     * 解析通用的字段
     * @param fieldMap 字段MAP对象
     * @param listFieldProp 列表字段属性对象
     * @return 如果解析成功返回true；否则返回false
     */
    public static boolean parseCommonField(Map<String, Object> fieldMap, AbstractListFieldProp listFieldProp) {
        String tableId = StringUtils.handleNull(fieldMap.get("bind_table"));
        String fieldId = StringUtils.handleNull(fieldMap.get("bind_table_field"));
        if(StringUtils.isEmpty(tableId) || StringUtils.isEmpty(fieldId)) {
            return false;
        }
        listFieldProp.setTableId(tableId);
        listFieldProp.setFieldId(fieldId);
        listFieldProp.setTitle(StringUtils.handleNull(fieldMap.get("title")));
        listFieldProp.setPluginType(StringUtils.handleNull(fieldMap.get("leipiplugins")));
        String supportSearch = StringUtils.handleNull(fieldMap.get("is_search_field"));
        String supportSort = StringUtils.handleNull(fieldMap.get("is_sort"));
        listFieldProp.setSupportSearch(YesNoType.getSupportDefaultObj(supportSearch).getValue());
        listFieldProp.setSupportSort(YesNoType.getSupportDefaultObj(supportSort).getValue());
        return true;
    }

    /**
     * 解析通用的选项属性
     * @param dataMap
     * @return
     */
    public static OptionListFieldProp parseCommonOptionProp(Map<String, Object> dataMap) {
        String fromData = StringUtils.handleNull(dataMap.get("from_data"));
        String dynamicLoad = StringUtils.handleNull(dataMap.get("dynamicload"));
        List<Map<String,Object>> options = (List<Map<String, Object>>) dataMap.get("options");

        OptionListFieldProp listFieldProp = new OptionListFieldProp();
        FormDataSourceType sourceType = FormDataSourceType.getObj(fromData);
        switch (sourceType) {
            case DICT:
            case CUSTOM_URI:
                if(YesNoType.YES.getStrValue().equals(dynamicLoad)) {
                    listFieldProp.setUrl(StringUtils.handleNull(dataMap.get("data_uri")));
                    break;
                }
            default:
                if(CollectionUtils.isNotEmpty(options)) {
                    List<LabelValue> labelValues = new ArrayList<>(options.size());
                    for (Map<String, Object> map : options) {
                        labelValues.add(new LabelValue(StringUtils.handleNull(map.get("text")), StringUtils.handleNull(map.get("value"))));
                    }
                    listFieldProp.setOptions(labelValues);
                }
                break;
        }
        if(FormListParseHelper.parseCommonField(dataMap, listFieldProp)) {
            return listFieldProp;
        }
        return null;
    }
    
}
