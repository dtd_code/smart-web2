package cn.com.smart.form.list.bean;

import java.util.List;

import cn.com.smart.web.bean.LabelValue;

/**
 * 选项列表属性（如：select、checkbox、radio等类型）
 * @author lmq
 *
 */
public class OptionListFieldProp extends AbstractListFieldProp {

    private String url;
    
    private List<LabelValue> options;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<LabelValue> getOptions() {
        return options;
    }

    public void setOptions(List<LabelValue> options) {
        this.options = options;
    }
    
}
