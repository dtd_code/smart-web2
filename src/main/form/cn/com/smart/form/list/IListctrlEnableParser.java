package cn.com.smart.form.list;

import java.util.Map;

/**
 * 有能力解析列表控件
 * @author 乌草坡 2019-09-03
 * @since 1.0
 */
public interface IListctrlEnableParser {

    /**
     * 获取列表控件标题
     * @param dataMap 字段数据MAP对象
     * @return 返回列表控件标题
     */
    String getListCtrlTitle(Map<String, Object> dataMap);

}
